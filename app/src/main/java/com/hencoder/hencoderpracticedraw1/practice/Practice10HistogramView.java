package com.hencoder.hencoderpracticedraw1.practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

public class Practice10HistogramView extends View {

    private Paint barPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint paint;
    private int gap = 20;
    private ArrayList<BarData> barList;


    public Practice10HistogramView(Context context) {
        super(context);
        init();
    }

    public Practice10HistogramView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Practice10HistogramView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        barPaint.setTextSize(30);
        barPaint.setColor(Color.WHITE);

        barList = new ArrayList<>();
        barList.add(new BarData("Froyo", 0.01f));
        barList.add(new BarData("GB", 0.05f));
        barList.add(new BarData("IC S", 0.05f));
        barList.add(new BarData("JB", 0.40f));
        barList.add(new BarData("KitKat", 0.70f));
        barList.add(new BarData("L", 0.80f));
        barList.add(new BarData("M", 0.35f));

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int height = canvas.getHeight();
        int width = canvas.getWidth();
        int unitHeight = (height / 10);
        int unitWidth = (width / 10);

        int baseHeight = unitHeight * 7;
        int baseWidth = unitWidth * 8 / 7 - 5;


//        综合练习
//        练习内容：使用各种 Canvas.drawXXX() 方法画直方图

        drawLine(canvas, unitHeight, unitWidth);
        setText(canvas, unitHeight, unitWidth);

        for (int i = 0; i < 7; i++) {
            BarData data = barList.get(i);
            paint.setColor(Color.GREEN);
            int left = gap + unitWidth + baseWidth * i;
            float top = unitHeight + ((baseHeight - unitHeight) * (1 - data.getValue()));
            int right = unitWidth + baseWidth * (i + 1);

            canvas.drawRect(left, top, right, baseHeight, paint);
            drawBarText(canvas, baseHeight, left, data.getName(), right-left);
        }
    }

    private void drawBarText(Canvas canvas, int baseHeight, int left, String dataName, int width) {
        Rect rect = new Rect();
        barPaint.getTextBounds(dataName, 0, dataName.length(), rect);
        canvas.drawText(dataName, left + ((width - rect.width()) / 2), baseHeight + rect.height(), barPaint);
    }

    private void drawLine(Canvas canvas, int unitHeight, int unitWidth) {
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        canvas.drawLine(unitWidth, unitHeight, unitWidth, unitHeight * 7, paint);
        canvas.drawLine(unitWidth, unitHeight * 7, unitWidth * 9, unitHeight * 7, paint);
    }

    private void setText(Canvas canvas, int unitHeight, int unitWidth) {
        paint.setTextSize(60);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(1);
        canvas.drawText("直方圖", unitWidth * 4, unitHeight * 9 + 5, paint);
    }

    class BarData {
        private String name;
        private float percentageValue;

        public BarData(String name, float percentageValue) {
            this.name = name;
            this.percentageValue = percentageValue;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getValue() {
            return percentageValue;
        }

        public void setValue(int value) {
            this.percentageValue = value;
        }
    }

}

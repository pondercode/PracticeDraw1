package com.hencoder.hencoderpracticedraw1.practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

public class Practice11PieChartView extends View {

    private Paint chartPaint;
    private Paint linePaint;
    private ArrayList<ChartData> chartList;
    private RectF ChartRect;
    private int chartLeft;
    private int chartTop;
    private int chartRight;
    private int chartBottom;
    private float radius;

    public Practice11PieChartView(Context context) {
        super(context);
        init();
    }

    public Practice11PieChartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Practice11PieChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        chartPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        linePaint.setColor(Color.WHITE);
        linePaint.setStrokeWidth(3);

        chartList = new ArrayList<>();
        chartList.add(new ChartData(Color.parseColor("#F44336"), "Marshmallow", 0.14f));
        chartList.add(new ChartData(Color.parseColor("#FFC107"), "Froyo", 0.02f));
        chartList.add(new ChartData(Color.parseColor("#9C27B0"), "Gingerbread", 0.10f));
        chartList.add(new ChartData(Color.parseColor("#6F8188"), "Ice Cream Sandwich", 0.03f));
        chartList.add(new ChartData(Color.parseColor("#009688"), "Jelly Bean", 0.02f));
        chartList.add(new ChartData(Color.parseColor("#2196F3"), "KitKat", 0.25f));
        chartList.add(new ChartData(Color.parseColor("#FF4081"), "Lollipop", 0.44f));

        ChartRect = new RectF();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int height = canvas.getHeight();
        int width = canvas.getWidth();

        canvas.translate(canvas.getWidth() / 2, canvas.getHeight() / 2);

        int i1 = (canvas.getHeight() / 5) * 2;

        ChartRect.left = -i1;
        ChartRect.top = -i1;
        ChartRect.right = i1;
        ChartRect.bottom = i1;
        radius = (ChartRect.right - ChartRect.left) / 2;
//        综合练习
//        练习内容：使用各种 Canvas.drawXXX() 方法画饼图

        float lastDegree = -45f;
        for (int i = 0; i < chartList.size(); i++) {
            ChartData data = chartList.get(i);
            chartPaint.setColor(data.getColor());
            chartPaint.setStyle(Paint.Style.FILL);

            float degree = data.getValue() * 360;
            float lineAngle = lastDegree + degree / 2;

            float lineStartX = radius * (float) Math.cos(lineAngle / 180 * Math.PI);
            float lineStartY = radius * (float) Math.sin(lineAngle / 180 * Math.PI);
            float lineEndX = (radius + 50) * (float) Math.cos(lineAngle / 180 * Math.PI);
            float lineEndY = (radius + 50) * (float) Math.sin(lineAngle / 180 * Math.PI);

            canvas.drawArc(ChartRect, lastDegree, degree, true, chartPaint);
            canvas.drawLine(lineStartX, lineStartY, lineEndX, lineEndY, linePaint);
            lastDegree += degree;
        }

    }


    class ChartData {
        private int color;
        private String name;
        private float value;

        public ChartData(int color, String name, float value) {
            this.color = color;
            this.name = name;
            this.value = value;
        }

        public int getColor() {
            return color;
        }

        public void setColor(int color) {
            this.color = color;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getValue() {
            return value;
        }

        public void setValue(float value) {
            this.value = value;
        }
    }
}

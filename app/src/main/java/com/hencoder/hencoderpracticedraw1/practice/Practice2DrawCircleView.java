package com.hencoder.hencoderpracticedraw1.practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class Practice2DrawCircleView extends View {
    Paint paint;

    public Practice2DrawCircleView(Context context) {
        super(context);
    }

    public Practice2DrawCircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Practice2DrawCircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        int height = canvas.getHeight();
        int width = canvas.getWidth();
        int triHeight = (height / 4) - 5;
        int triWidth = (width / 4) - 5;

//        练习内容：使用 canvas.drawCircle() 方法画圆
//        一共四个圆：1.实心圆 2.空心圆 3.蓝色实心圆 4.线宽为 20 的空心圆


        //1.实心圆
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(triWidth, triHeight, triHeight, paint);

        //2.空心圆
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        canvas.drawCircle(triWidth * 3, triHeight, triHeight, paint);

        //3.蓝色实心圆
        paint.setColor(Color.BLUE);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(triWidth, triHeight * 3, triHeight, paint);

        //4.线宽为 20 的空心圆
//        paint.setColor(Color.BLACK);
//        paint.setStyle(Paint.Style.FILL);
//        Path path = new Path();
//        path.setFillType(Path.FillType.WINDING);
//        path.addCircle(triWidth * 3, triHeight * 3, triHeight+20, Path.Direction.CCW);
//        path.addCircle(triWidth * 3, triHeight * 3, triHeight, Path.Direction.CW);
//        canvas.drawPath(path, paint);

        paint.setStrokeWidth(20);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(triWidth * 3, triHeight * 3, triHeight, paint);
    }
}
